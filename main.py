import json
import logging
import os
import functools

import yaml
from kubernetes import client, config
from paho.mqtt import client as mqtt_client


# Setup logging
logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
logger = logging.getLogger(__name__)

# Read configuration from a file - CONFIG_PATH variable comes from the kube-runner.service file
with open(os.getenv('CONFIG_PATH'), 'r') as config_file:
    config_data = yaml.safe_load(config_file)
    logger.info("Config file loaded")

# Create additional variables to shorten variable invocation in the code
mqtt_data = config_data['mqtt']
k8s_config_path = config_data['k8s']['kube_config_path']
k8s_namespace = config_data['k8s']['namespace']
k8s_deployment_template = config_data['k8s']['deployment_template']
k8s_apps = config_data['k8s']['apps']


# Create Kubernetes api client for interacting with a Kubernetes API Server
def get_k8s_api_client():
    config.load_kube_config(k8s_config_path)
    return client.AppsV1Api()


# Check if gps app is staying in one place for a while
def is_lazy():
    pass


# Create mqtt client and connect it with specified broker
def connect_mqtt():
    def on_connect(client, userdata, flags, rc, properties):
        if rc == 0:
            logger.info("Connected to MQTT Broker!")
        else:
            logger.error("Failed to connect, return code %d\n", rc)
 
    client = mqtt_client.Client(client_id="kube_runner", callback_api_version=mqtt_client.CallbackAPIVersion.VERSION2)
    client.on_connect = on_connect
    client.connect(mqtt_data['ip'], mqtt_data['port'])
    logger.info("MQTT Client created")
    return client


# Main function used in an infinite loop
def subscribe(mqtt_client, k8s_client):
    def on_message(mqtt_client, userdata, msg, k8s_client):
        print(msg.payload.decode())
        msg_data = json.loads(msg.payload.decode())
        logger.info("Received `%s` from `%s` topic", msg_data, msg.topic)
        k8s_deployment_name = k8s_deployment_template + msg_data["deviceid"]
        
        # Anti-theft App
        if msg_data['sensor'] == "acc":
            logger.info("Detected anti-theft app")
            if msg_data['data']['acc3d'] >= k8s_apps['guard']['threshold']:
                logger.info("App change detected")
                update_deployment_image(k8s_client, k8s_deployment_name, k8s_apps['on_alarm']['image'], k8s_namespace)
            else:
                logger.info("acceleration below the threshold")

        # TelemetryMonitor App
        elif msg_data['sensor'] == 'geo':
            logger.info("Detected gps-app")
            if msg_data['data']['speed'] >= k8s_apps['on_alarm']['threshold']:
                update_deployment_image(k8s_client, k8s_deployment_name, k8s_apps['guard']['image'], k8s_namespace)
            else:
                logger.info("gps app localization is still moving")
        else:
            logger.error("App name not recognized!: %s", msg_data['app'])
    
    # Add k8s_client keyword argument as a default one
    partial_on_message = functools.partial(on_message, k8s_client=k8s_client)

    mqtt_client.subscribe(mqtt_data['topic'])
    mqtt_client.on_message = partial_on_message


# Update image name in kubernetes deployment definition
def update_deployment_image(api_instance, deployment_name, new_image, namespace):

    # get architecture of the existing image in a deployment
    def get_image_arch(api_instance, deployment_name, namespace):
        deployment = api_instance.read_namespaced_deployment(name=deployment_name, namespace=namespace)
        return deployment.spec.template.spec.containers[0].image.split(":")[1]

    body = {
        "spec": {
            "template": {
                "spec": {
                    "containers": [
                        {
                            "name": deployment_name,
                            "image": new_image + ":" + get_image_arch(api_instance, deployment_name, namespace)
                        }
                    ]
                }
            }
        }
    }
    api_response = api_instance.patch_namespaced_deployment(
        name=deployment_name,
        namespace=namespace,
        body=body
    )
    logger.info("Deployment definition updated")


def run_service():
    k8s_client = get_k8s_api_client()
    mqtt_client = connect_mqtt()
    subscribe(mqtt_client, k8s_client)
    mqtt_client.loop_forever()

if __name__ == '__main__':
    run_service()
    
